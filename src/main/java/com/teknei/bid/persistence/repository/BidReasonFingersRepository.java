package com.teknei.bid.persistence.repository; 
import org.springframework.data.jpa.repository.JpaRepository; 
import com.teknei.bid.persistence.entities.BidReasonFingers;

public interface BidReasonFingersRepository  extends JpaRepository<BidReasonFingers, String>{

}
