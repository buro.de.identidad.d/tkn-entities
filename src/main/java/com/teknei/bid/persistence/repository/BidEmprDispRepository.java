package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidEmprDisp;
import com.teknei.bid.persistence.entities.BidEmprDispPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidEmprDispRepository extends JpaRepository<BidEmprDisp, BidEmprDispPK> {

    List<BidEmprDisp> findAllByIdDisp(Long idDisp);

}