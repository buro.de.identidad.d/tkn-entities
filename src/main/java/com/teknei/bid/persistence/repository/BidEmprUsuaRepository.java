package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidEmprUsua;
import com.teknei.bid.persistence.entities.BidEmprUsuaPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidEmprUsuaRepository extends JpaRepository<BidEmprUsua, BidEmprUsuaPK> {

    List<BidEmprUsua> findAllByIdUsua(Long idUsua);

}