package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidDocuPrim;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidDocuPrimRepository extends JpaRepository<BidDocuPrim, Long> {
}
