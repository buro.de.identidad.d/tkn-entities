package com.teknei.bid.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.CfgGeneralConfiguration;

public interface CfgGeneralConfigurationRepository extends JpaRepository<CfgGeneralConfiguration, Long> {

	CfgGeneralConfiguration findByParameter(String parameter);

	List<CfgGeneralConfiguration> findByCfgGroup(String group);

}
