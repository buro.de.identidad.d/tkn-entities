package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidTipoPlat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidTipoPlatRepository extends JpaRepository<BidTipoPlat, Long> {

    List<BidTipoPlat> findAllByIdEsta(Integer idEsta);
}