package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidUsua;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BidUsuaRepository extends JpaRepository<BidUsua, Long> {

    List<BidUsua> findByIdEsta(int idEsta);

    List<BidUsua> findByUsua(String usua);
    
    List<BidUsua> findByIdClie(Long idClie);
     
        
    @Query(value = "SELECT * FROM bid.bid_usua ORDER BY id_usua DESC LIMIT 1", nativeQuery = true)
    BidUsua findlastUsua();

}