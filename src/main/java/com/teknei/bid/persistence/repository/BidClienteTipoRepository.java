package com.teknei.bid.persistence.repository;
import com.teknei.bid.persistence.entities.BidClienteTipo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;  

public interface BidClienteTipoRepository extends JpaRepository<BidClienteTipo,Long> 
{
	List<BidClienteTipo>  findAllByEstatus(String estatus);	
}
	