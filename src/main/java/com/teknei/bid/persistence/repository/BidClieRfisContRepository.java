package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieRfisCont;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieRfisContRepository extends JpaRepository<BidClieRfisCont, Long> {

    List<BidClieRfisCont> findAllByIdClie(Long idClie);

}