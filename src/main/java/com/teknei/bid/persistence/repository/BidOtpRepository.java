package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidOtp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidOtpRepository extends JpaRepository<BidOtp, Long> {

    List<BidOtp> findByOtpAndUsed(String otp, Boolean used);

    List<BidOtp> findByUsed(Boolean used);

    List<BidOtp> findBySend(Boolean send);

    List<BidOtp> findByIdClieAndOtpAndUsed(Long idClie, String otp, Boolean used);

    List<BidOtp> findByIdClie(Long idClie);

    List<BidOtp> findByIdClieAndUsed(Long idClie, Boolean used);
}
