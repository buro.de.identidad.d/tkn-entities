package com.teknei.bid.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.BidValUsr;

/**
 * 
 * @author AJGD
 *
 */
public interface BidValUsrRepository extends JpaRepository<BidValUsr, Long> {

	BidValUsr findByUser(long user);

}