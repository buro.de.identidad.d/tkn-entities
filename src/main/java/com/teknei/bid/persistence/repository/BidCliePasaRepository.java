package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidCliePasa;
import com.teknei.bid.persistence.entities.BidCliePasaPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidCliePasaRepository extends JpaRepository<BidCliePasa, BidCliePasaPK> {
}
