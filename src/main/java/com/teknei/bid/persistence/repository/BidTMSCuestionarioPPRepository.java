package com.teknei.bid.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.TMSCuestionarioPP;

public interface BidTMSCuestionarioPPRepository extends JpaRepository<TMSCuestionarioPP, Long> {

}
