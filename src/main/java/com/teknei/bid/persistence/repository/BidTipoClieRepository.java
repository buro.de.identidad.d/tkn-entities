package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidTipoClie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidTipoClieRepository extends JpaRepository<BidTipoClie, Long> {
}
