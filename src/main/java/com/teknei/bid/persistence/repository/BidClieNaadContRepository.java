package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieNaadCont;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieNaadContRepository extends JpaRepository<BidClieNaadCont, Long> {

    List<BidClieNaadCont> findAllByIdClie(Long idClie);

}