package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieRegProc;
import com.teknei.bid.persistence.entities.BidClieRegProcPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface BidRegProcRepository extends JpaRepository<BidClieRegProc, BidClieRegProcPK> {

    BidClieRegProc findTopByIdClie(Long idClie);

    BidClieRegProc findTopByCurp(String curp);

    @Deprecated
    List<BidClieRegProc> findAllByRegIneAndRegFaciAndRegDomiAndRegDactAndFchRegIneBetweenAndFchRegFaciBetweenAndFchRegDomiBetween(Boolean regIne, Boolean regFaci, Boolean regDomi, Boolean regDact, Timestamp fchRegIneBefore, Timestamp fchRefIneAfter, Timestamp fchRegFaciBefore, Timestamp fchRegFaciAfter, Timestamp fchRegiDomiBefore, Timestamp fchRegiDomiAfter);

    @Deprecated
    List<BidClieRegProc> findAllByRegIneAndRegFaciAndRegDomiAndRegDactAndFchRegIneBetweenAndFchRegFaciBetween(Boolean regIne, Boolean regFaci, Boolean regDomi, Boolean regDact, Timestamp fchRegIneBefore, Timestamp fchRefIneAfter, Timestamp fchRegFaciBefore, Timestamp fchRegFaciAfter);

    List<BidClieRegProc> findAllByRegIneAndRegFaciAndRegDomiAndRegDactAndFchRegIneBetween(Boolean regIne, Boolean regFaci, Boolean regDomi, Boolean regDact, Timestamp fchRegIneBefore, Timestamp fchRefIneAfter);

    List<BidClieRegProc> findAllByRegIneAndRegFaciAndRegDomiAndRegDactAndRegContAndFchRegIneBetween(Boolean regIne, Boolean regFaci, Boolean regDomi, Boolean regDact, Boolean regCont, Timestamp fchRegIneBefore, Timestamp fchRefIneAfter);

    BidClieRegProc findTopByIdClieAndRegDact(Long idClie, Boolean regDact);

    @Modifying
    @Query("UPDATE BidClieRegProc b SET b.curp = :curp WHERE b.idClie = :idClie")
    void updateCurp(@Param("curp") String curp, @Param("idClie") Long idClie);
}
