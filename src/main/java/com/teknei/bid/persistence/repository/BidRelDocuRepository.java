package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidRelDocu;
import com.teknei.bid.persistence.entities.BidRelDocuPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BidRelDocuRepository extends JpaRepository<BidRelDocu, BidRelDocuPK> {

    List<BidRelDocu> findAllByIdEstaAndIdDocuPrim(Integer idEsta, Long idDocuPrim);

    @Query("select distinct b.idDocuPrim FROM BidRelDocu b where b.idEsta = :idEsta")
    List<Long> findDistinctIdDocuPrim(@Param("idEsta") Integer idEsta);

}
