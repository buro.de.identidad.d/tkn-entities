package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidConsSist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidConsSistRepository extends JpaRepository<BidConsSist, Long> {

    List<BidConsSist> findAllByCodConsSistAndIdEsta(String codConsSist, Integer idEsta);

}