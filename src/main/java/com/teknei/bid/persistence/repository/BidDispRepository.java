package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidDisp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidDispRepository extends JpaRepository<BidDisp, Long> {

    List<BidDisp> findByIdEsta(Integer idEsta);

}