package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidEmpr;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidEmprRepository extends JpaRepository<BidEmpr, Long> {

    List<BidEmpr> findByIdEsta(Integer idEsta);

    BidEmpr findByIdEmprAndIdEsta(Long idEmpr, Integer idEsta);

}