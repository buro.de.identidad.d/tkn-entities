package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidPais;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidPaisRepository extends JpaRepository<BidPais, String> {
}