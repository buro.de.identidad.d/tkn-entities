package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieCurpPK;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public abstract interface BidCurpRepository
  extends JpaRepository<BidClieCurp, BidClieCurpPK>
{
  public abstract BidClieCurp findFirstByCurp(String paramString);
  
  public abstract List<BidClieCurp> findAllByCurp(String paramString);
  
  public abstract BidClieCurp findTopByCurpAndIdTipo(String paramString, Integer paramInteger);
  
  public abstract BidClieCurp findTopByIdClie(Long paramLong);
  
  @Modifying
  @Query("UPDATE BidClieCurp b SET b.curp = :curp WHERE b.idClie = :idClie")
  public abstract void updateCurp(@Param("curp") String paramString, @Param("idClie") Long paramLong);
  
  @Modifying
  @Query("UPDATE BidClieCurp b SET b.idTipo = :idTipo WHERE b.idClie = :idClie")
  public abstract void updateIdTipo(@Param("idClie") Long paramLong, @Param("idTipo") Integer paramInteger);
  
  @Modifying
  @Query("UPDATE BidClieCurp b SET b.rel = :rel WHERE b.idClie = :idClie")
  public abstract void updateIdclierel(@Param("idClie") Long paramLong1, @Param("rel") Long paramLong2);
}