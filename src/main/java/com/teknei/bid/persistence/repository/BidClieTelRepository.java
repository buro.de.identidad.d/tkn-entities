package com.teknei.bid.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teknei.bid.persistence.entities.BidClieTel;

public interface BidClieTelRepository extends JpaRepository<BidClieTel, Long> {

	    BidClieTel findByIdClie(Long idClie);
}
