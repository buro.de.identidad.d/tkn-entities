package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieDire;
import com.teknei.bid.persistence.entities.BidClieDirePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidDireRepository extends JpaRepository<BidClieDire, BidClieDirePK> {

    BidClieDire findFirstByIdClieAndIdEsta(Long idClie, Integer idEsta);

}