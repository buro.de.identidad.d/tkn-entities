package com.teknei.bid.persistence.repository;
import com.teknei.bid.persistence.entities.BidClieQr;
import org.springframework.data.jpa.repository.JpaRepository;
public interface BidClieQrRepository  extends JpaRepository<BidClieQr, String>  
{
	  public  BidClieQr findByQrAndIdEstaQr(String s, Integer integer);
}
