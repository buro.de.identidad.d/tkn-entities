package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieDact;
import com.teknei.bid.persistence.entities.BidClieDactPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidDactRepository extends JpaRepository<BidClieDact, BidClieDactPK> {
}
