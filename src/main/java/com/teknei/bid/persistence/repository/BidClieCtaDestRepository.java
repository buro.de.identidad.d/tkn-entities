package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieCtaDest;
import com.teknei.bid.persistence.entities.BidClieCtaDestPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieCtaDestRepository extends JpaRepository<BidClieCtaDest, BidClieCtaDestPK> {

    List<BidClieCtaDest> findAllByIdClieAndIdEsta(Long idClie, Integer idEsta);

}