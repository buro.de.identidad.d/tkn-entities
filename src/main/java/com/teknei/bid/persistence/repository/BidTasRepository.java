package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieTas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidTasRepository extends JpaRepository<BidClieTas, Long> {

    BidClieTas findByIdClie(Long idClie);

    BidClieTas findByIdTas(String idTas);

}