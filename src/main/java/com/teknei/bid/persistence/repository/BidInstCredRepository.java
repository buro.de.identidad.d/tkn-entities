package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidInstCred;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BidInstCredRepository extends JpaRepository<BidInstCred, Long> {
	
	 @Query(value = "SELECT n.* FROM bid.bid_inst_cred n WHERE n.num_abm IS NOT NULL AND n.num_abm <> '' ORDER BY n.num_abm", nativeQuery = true)
	 List<BidInstCred> findAllByAbmNotNull1();
	 
}
