package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_inst_cred", schema = "bid", catalog = "bid")
public class BidInstCred {
    private long idInstCred;
    private String abm;
    private String nomCort;
    private String nomLarg;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @SequenceGenerator(schema = "bid", sequenceName = "bid.bid_inst_cred_id_inst_cred_seq", allocationSize = 1, name = "bid_inst_cred_gen", catalog = "bid_db")
    @GeneratedValue(generator = "bid_inst_cred_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_inst_cred")
    public long getIdInstCred() {
        return idInstCred;
    }

    public void setIdInstCred(long idInstCred) {
        this.idInstCred = idInstCred;
    }

    @Basic
    @Column(name = "num_abm")
    public String getAbm() {
        return abm;
    }

    public void setAbm(String abm) {
        this.abm = abm;
    }
    
    @Basic
    @Column(name = "nom_cort")
    public String getNomCort() {
        return nomCort;
    }

    public void setNomCort(String nomCort) {
        this.nomCort = nomCort;
    }

    @Basic
    @Column(name = "nom_larg")
    public String getNomLarg() {
        return nomLarg;
    }

    public void setNomLarg(String nomLarg) {
        this.nomLarg = nomLarg;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidInstCred that = (BidInstCred) o;
        return idInstCred == that.idInstCred &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(nomCort, that.nomCort) &&
                Objects.equals(nomLarg, that.nomLarg) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idInstCred, nomCort, nomLarg, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
