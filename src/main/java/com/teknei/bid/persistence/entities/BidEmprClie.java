package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_empr_clie", schema = "bid", catalog = "bid")
@IdClass(BidEmprCliePK.class)
public class BidEmprClie {
    private long idEmpr;
    private long idClie;
    private int idTipo;
    private int idEsta;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_empr")
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEmprClie that = (BidEmprClie) o;
        return idEmpr == that.idEmpr &&
                idClie == that.idClie &&
                idTipo == that.idTipo &&
                idEsta == that.idEsta &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, idClie, idTipo, idEsta, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
