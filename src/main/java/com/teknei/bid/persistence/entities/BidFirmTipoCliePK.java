package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidFirmTipoCliePK implements Serializable {
    private Long idTipoFirm;
    private Long idTipoClie;

    @Column(name = "id_tipo_firm")
    @Id
    public Long getIdTipoFirm() {
        return idTipoFirm;
    }

    public void setIdTipoFirm(Long idTipoFirm) {
        this.idTipoFirm = idTipoFirm;
    }

    @Column(name = "id_tipo_clie")
    @Id
    public Long getIdTipoClie() {
        return idTipoClie;
    }

    public void setIdTipoClie(Long idTipoClie) {
        this.idTipoClie = idTipoClie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidFirmTipoCliePK that = (BidFirmTipoCliePK) o;
        return Objects.equals(idTipoFirm, that.idTipoFirm) &&
                Objects.equals(idTipoClie, that.idTipoClie);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTipoFirm, idTipoClie);
    }
}
