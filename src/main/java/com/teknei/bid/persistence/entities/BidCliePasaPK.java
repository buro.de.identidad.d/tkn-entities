package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidCliePasaPK implements Serializable {
    private long idClie;
    private long idPasa;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_pasa")
    @Id
    public long getIdPasa() {
        return idPasa;
    }

    public void setIdPasa(long idPasa) {
        this.idPasa = idPasa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidCliePasaPK pasaPK = (BidCliePasaPK) o;
        return idClie == pasaPK.idClie &&
                idPasa == pasaPK.idPasa;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idPasa);
    }
}
