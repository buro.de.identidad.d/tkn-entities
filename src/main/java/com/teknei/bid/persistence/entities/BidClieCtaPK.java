package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieCtaPK implements Serializable {
    private long idClie;
    private String ctaCorr;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "cta_corr")
    @Id
    public String getCtaCorr() {
        return ctaCorr;
    }

    public void setCtaCorr(String ctaCorr) {
        this.ctaCorr = ctaCorr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCtaPK that = (BidClieCtaPK) o;
        return idClie == that.idClie &&
                Objects.equals(ctaCorr, that.ctaCorr);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, ctaCorr);
    }
}
