package com.teknei.bid.persistence.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "bid_clie_reason_finger", schema = "bid", catalog = "bid")
public class BidClieReasonFinger {
	
	private Long id;
	private Long idClie;
	private String code;
	private String finger;
	private String description;	

	@Id	
	@SequenceGenerator(schema = "bid", name = "BID_CLIE_REASON_FINGER_SEQ", sequenceName = "bid.bid_clie_reason_finger_seq", allocationSize = 1, catalog = "bid")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_CLIE_REASON_FINGER_SEQ")
    @Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "idclie")
	public Long getIdClie() {
		return idClie;
	}

	public void setIdClie(Long idClie) {
		this.idClie = idClie;
	}

	@Basic
	@Column(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Basic
	@Column(name = "finger")
	public String getFinger() {
		return finger;
	}

	public void setFinger(String finger) {
		this.finger = finger;
	}

	@Basic
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
