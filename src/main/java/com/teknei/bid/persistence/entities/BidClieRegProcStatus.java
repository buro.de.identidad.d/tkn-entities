package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_reg_proc_status", schema = "bid", catalog = "bid")
public class BidClieRegProcStatus {
    private Long idClie;
    private String curp;
    private long idRegProcStatus;
    private Long idStatus;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "curp")
    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Id
    @SequenceGenerator(name = "seq_id_proc_status", sequenceName = "bid.bid_clie_reg_proc_status_id_reg_proc_status_seq", allocationSize = 1, schema = "bid", catalog = "bid")
    @GeneratedValue(generator = "seq_id_proc_status", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_reg_proc_status")
    public long getIdRegProcStatus() {
        return idRegProcStatus;
    }

    public void setIdRegProcStatus(long idRegProcStatus) {
        this.idRegProcStatus = idRegProcStatus;
    }

    @Basic
    @Column(name = "id_status")
    public Long getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Long idStatus) {
        this.idStatus = idStatus;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieRegProcStatus that = (BidClieRegProcStatus) o;
        return idRegProcStatus == that.idRegProcStatus &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(idClie, that.idClie) &&
                Objects.equals(curp, that.curp) &&
                Objects.equals(idStatus, that.idStatus) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, curp, idRegProcStatus, idStatus, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
