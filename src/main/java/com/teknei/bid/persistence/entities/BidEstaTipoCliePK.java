package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidEstaTipoCliePK implements Serializable {
    private long idEstaProc;
    private long idTipoClie;

    @Column(name = "id_esta_proc")
    @Id
    public long getIdEstaProc() {
        return idEstaProc;
    }

    public void setIdEstaProc(long idEstaProc) {
        this.idEstaProc = idEstaProc;
    }

    @Column(name = "id_tipo_clie")
    @Id
    public long getIdTipoClie() {
        return idTipoClie;
    }

    public void setIdTipoClie(long idTipoClie) {
        this.idTipoClie = idTipoClie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEstaTipoCliePK that = (BidEstaTipoCliePK) o;
        return idEstaProc == that.idEstaProc &&
                idTipoClie == that.idTipoClie;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEstaProc, idTipoClie);
    }
}
