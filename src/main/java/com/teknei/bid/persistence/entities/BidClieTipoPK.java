package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieTipoPK implements Serializable {
    private long idClie;
    private int idTipoClie;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_tipo_clie")
    @Id
    public int getIdTipoClie() {
        return idTipoClie;
    }

    public void setIdTipoClie(int idTipoClie) {
        this.idTipoClie = idTipoClie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieTipoPK that = (BidClieTipoPK) o;
        return idClie == that.idClie &&
                idTipoClie == that.idTipoClie;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idTipoClie);
    }
}
