package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_loca", schema = "bid", catalog = "bid")
@IdClass(BidLocaPK.class)
public class BidLoca {
    private String idPais;
    private String idEstd;
    private String idMuni;
    private String idLoca;
    private String nomLoca;

    @Id
    @Column(name = "id_pais")
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Id
    @Column(name = "id_estd")
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Id
    @Column(name = "id_muni")
    public String getIdMuni() {
        return idMuni;
    }

    public void setIdMuni(String idMuni) {
        this.idMuni = idMuni;
    }

    @Id
    @Column(name = "id_loca")
    public String getIdLoca() {
        return idLoca;
    }

    public void setIdLoca(String idLoca) {
        this.idLoca = idLoca;
    }

    @Basic
    @Column(name = "nom_loca")
    public String getNomLoca() {
        return nomLoca;
    }

    public void setNomLoca(String nomLoca) {
        this.nomLoca = nomLoca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidLoca bidLoca = (BidLoca) o;
        return Objects.equals(idPais, bidLoca.idPais) &&
                Objects.equals(idEstd, bidLoca.idEstd) &&
                Objects.equals(idMuni, bidLoca.idMuni) &&
                Objects.equals(idLoca, bidLoca.idLoca) &&
                Objects.equals(nomLoca, bidLoca.nomLoca);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd, idMuni, idLoca, nomLoca);
    }
}
