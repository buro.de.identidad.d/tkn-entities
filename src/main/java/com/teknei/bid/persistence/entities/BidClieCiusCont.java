package com.teknei.bid.persistence.entities;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Immutable
@Table(name = "bid_clie_cius_cont", schema = "bid", catalog = "bid")
public class BidClieCiusCont {
    private Long idClie;
    private String ciudAdi;
    private String myKey;

    @Basic
    @Column(name = "my_key")
    @Id
    public String getMyKey() {
        return myKey;
    }

    public void setMyKey(String myKey) {
        this.myKey = myKey;
    }

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "ciud_adi")
    public String getCiudAdi() {
        return ciudAdi;
    }

    public void setCiudAdi(String ciudAdi) {
        this.ciudAdi = ciudAdi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCiusCont that = (BidClieCiusCont) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(ciudAdi, that.ciudAdi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, ciudAdi);
    }
}
