package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieCertPK implements Serializable {
    private long idClie;
    private String nomUsua;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "nom_usua")
    @Id
    public String getNomUsua() {
        return nomUsua;
    }

    public void setNomUsua(String nomUsua) {
        this.nomUsua = nomUsua;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCertPK that = (BidClieCertPK) o;
        return idClie == that.idClie &&
                Objects.equals(nomUsua, that.nomUsua);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, nomUsua);
    }
}
