package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidCelPK implements Serializable {
    private long idClie;
    private String numCel;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "num_cel")
    @Id
    public String getNumCel() {
        return numCel;
    }

    public void setNumCel(String numCel) {
        this.numCel = numCel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidCelPK bidTelPK = (BidCelPK) o;
        return idClie == bidTelPK.idClie &&
                Objects.equals(numCel, bidTelPK.numCel);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, numCel);
    }
}

