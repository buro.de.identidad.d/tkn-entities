package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidRelDocuPK implements Serializable {
    private long idDocuPrim;
    private long idDocuSecu;

    @Column(name = "id_docu_prim")
    @Id
    public long getIdDocuPrim() {
        return idDocuPrim;
    }

    public void setIdDocuPrim(long idDocuPrim) {
        this.idDocuPrim = idDocuPrim;
    }

    @Column(name = "id_docu_secu")
    @Id
    public long getIdDocuSecu() {
        return idDocuSecu;
    }

    public void setIdDocuSecu(long idDocuSecu) {
        this.idDocuSecu = idDocuSecu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidRelDocuPK that = (BidRelDocuPK) o;
        return idDocuPrim == that.idDocuPrim &&
                idDocuSecu == that.idDocuSecu;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDocuPrim, idDocuSecu);
    }
}
