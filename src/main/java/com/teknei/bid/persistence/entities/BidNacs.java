package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_nacs", schema = "bid", catalog = "bid")
public class BidNacs {
    private long idNacs;
    private String nomNacs;

    @Id
    @Column(name = "id_nacs")
    public long getIdNacs() {
        return idNacs;
    }

    public void setIdNacs(long idNacs) {
        this.idNacs = idNacs;
    }

    @Basic
    @Column(name = "nom_nacs")
    public String getNomNacs() {
        return nomNacs;
    }

    public void setNomNacs(String nomNacs) {
        this.nomNacs = nomNacs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidNacs bidNacs = (BidNacs) o;
        return idNacs == bidNacs.idNacs &&
                Objects.equals(nomNacs, bidNacs.nomNacs);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idNacs, nomNacs);
    }
}
