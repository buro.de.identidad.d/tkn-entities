package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_banc", schema = "bid", catalog = "bid")
public class BidClieBanc {
    private long idClie;
    private String numeClie;
    private String acti;
    private String ingMensBrt;
    private boolean pepoExp;
    private boolean parePepoExp;
    private String nombPepoExp;
    private Long idPare;
    private String docuIden;
    private String numeDocuIden;
    private String numeSefe;
    private String funcBanc;
    private boolean ciudEua;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Basic
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "nume_clie")
    public String getNumeClie() {
        return numeClie;
    }

    public void setNumeClie(String numeClie) {
        this.numeClie = numeClie;
    }

    @Basic
    @Column(name = "acti")
    public String getActi() {
        return acti;
    }

    public void setActi(String acti) {
        this.acti = acti;
    }

    @Basic
    @Column(name = "ing_mens_brt")
    public String getIngMensBrt() {
        return ingMensBrt;
    }

    public void setIngMensBrt(String ingMensBrt) {
        this.ingMensBrt = ingMensBrt;
    }

    @Basic
    @Column(name = "pepo_exp")
    public boolean isPepoExp() {
        return pepoExp;
    }

    public void setPepoExp(boolean pepoExp) {
        this.pepoExp = pepoExp;
    }

    @Basic
    @Column(name = "pare_pepo_exp")
    public boolean isParePepoExp() {
        return parePepoExp;
    }

    public void setParePepoExp(boolean parePepoExp) {
        this.parePepoExp = parePepoExp;
    }

    @Basic
    @Column(name = "nomb_pepo_exp")
    public String getNombPepoExp() {
        return nombPepoExp;
    }

    public void setNombPepoExp(String nombPepoExp) {
        this.nombPepoExp = nombPepoExp;
    }

    @Basic
    @Column(name = "id_pare")
    public Long getIdPare() {
        return idPare;
    }

    public void setIdPare(Long idPare) {
        this.idPare = idPare;
    }

    @Basic
    @Column(name = "docu_iden")
    public String getDocuIden() {
        return docuIden;
    }

    public void setDocuIden(String docuIden) {
        this.docuIden = docuIden;
    }

    @Basic
    @Column(name = "nume_docu_iden")
    public String getNumeDocuIden() {
        return numeDocuIden;
    }

    public void setNumeDocuIden(String numeDocuIden) {
        this.numeDocuIden = numeDocuIden;
    }

    @Basic
    @Column(name = "nume_sefe")
    public String getNumeSefe() {
        return numeSefe;
    }

    public void setNumeSefe(String numeSefe) {
        this.numeSefe = numeSefe;
    }

    @Basic
    @Column(name = "func_banc")
    public String getFuncBanc() {
        return funcBanc;
    }

    public void setFuncBanc(String funcBanc) {
        this.funcBanc = funcBanc;
    }

    @Basic
    @Column(name = "ciud_eua")
    public boolean isCiudEua() {
        return ciudEua;
    }

    public void setCiudEua(boolean ciudEua) {
        this.ciudEua = ciudEua;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieBanc that = (BidClieBanc) o;
        return idClie == that.idClie &&
                pepoExp == that.pepoExp &&
                parePepoExp == that.parePepoExp &&
                ciudEua == that.ciudEua &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(numeClie, that.numeClie) &&
                Objects.equals(acti, that.acti) &&
                Objects.equals(ingMensBrt, that.ingMensBrt) &&
                Objects.equals(nombPepoExp, that.nombPepoExp) &&
                Objects.equals(idPare, that.idPare) &&
                Objects.equals(docuIden, that.docuIden) &&
                Objects.equals(numeDocuIden, that.numeDocuIden) &&
                Objects.equals(numeSefe, that.numeSefe) &&
                Objects.equals(funcBanc, that.funcBanc) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, numeClie, acti, ingMensBrt, pepoExp, parePepoExp, nombPepoExp, idPare, docuIden, numeDocuIden, numeSefe, funcBanc, ciudEua, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
