package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieBenePK implements Serializable {
    private long idClie;
    private long idPare;
    private long idClieBene;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_pare")
    @Id
    public long getIdPare() {
        return idPare;
    }

    public void setIdPare(long idPare) {
        this.idPare = idPare;
    }

    @Column(name = "id_clie_bene")
    @Id
    public long getIdClieBene() {
        return idClieBene;
    }

    public void setIdClieBene(long idClieBene) {
        this.idClieBene = idClieBene;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieBenePK that = (BidClieBenePK) o;
        return idClie == that.idClie &&
                idPare == that.idPare &&
                idClieBene == that.idClieBene;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idPare, idClieBene);
    }
}
