package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie", schema = "bid", catalog = "bid")
public class BidClie {
    private long idClie;
    private String nomClie;
    private String apePate;
    private String apeMate;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String geneClie;
    private String fchNac;
    private String rfc;
    private String lugNac;
    private String paisNac;
    private String naci;
    private String usrOpeCrea;
    private String usrOpeModi;
    private long idclierel;

    @Id
    @SequenceGenerator(schema = "bid", sequenceName = "bid.bid_clie_id_clie_seq", allocationSize = 1, name = "bid_clie_gen", catalog = "bid_db")
    @GeneratedValue(generator = "bid_clie_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "nom_clie")
    public String getNomClie() {
        return nomClie;
    }

    public void setNomClie(String nomClie) {
        this.nomClie = nomClie;
    }

    @Basic
    @Column(name = "ape_pate")
    public String getApePate() {
        return apePate;
    }

    public void setApePate(String apePate) {
        this.apePate = apePate;
    }

    @Basic
    @Column(name = "ape_mate")
    public String getApeMate() {
        return apeMate;
    }

    public void setApeMate(String apeMate) {
        this.apeMate = apeMate;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "gene_clie")
    public String getGeneClie() {
        return geneClie;
    }

    public void setGeneClie(String geneClie) {
        this.geneClie = geneClie;
    }

    @Basic
    @Column(name = "fch_nac")
    public String getFchNac() {
        return fchNac;
    }

    public void setFchNac(String fchNac) {
        this.fchNac = fchNac;
    }

    @Basic
    @Column(name = "rfc")
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @Basic
    @Column(name = "lug_nac")
    public String getLugNac() {
        return lugNac;
    }

    public void setLugNac(String lugNac) {
        this.lugNac = lugNac;
    }

    @Basic
    @Column(name = "pais_nac")
    public String getPaisNac() {
        return paisNac;
    }

    public void setPaisNac(String paisNac) {
        this.paisNac = paisNac;
    }

    @Basic
    @Column(name = "naci")
    public String getNaci() {
        return naci;
    }

    public void setNaci(String naci) {
        this.naci = naci;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Basic
    @Column(name = "id_clie_rel")
	public long getIdclierel() {
		return idclierel;
	}

	public void setIdclierel(long idclierel) {
		this.idclierel = idclierel;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClie bidClie = (BidClie) o;
        return idClie == bidClie.idClie &&
                idEsta == bidClie.idEsta &&
                idclierel == bidClie.idclierel &&
                idTipo == bidClie.idTipo &&
                Objects.equals(nomClie, bidClie.nomClie) &&
                Objects.equals(apePate, bidClie.apePate) &&
                Objects.equals(apeMate, bidClie.apeMate) &&
                Objects.equals(usrCrea, bidClie.usrCrea) &&
                Objects.equals(fchCrea, bidClie.fchCrea) &&
                Objects.equals(usrModi, bidClie.usrModi) &&
                Objects.equals(fchModi, bidClie.fchModi) &&
                Objects.equals(geneClie, bidClie.geneClie) &&
                Objects.equals(fchNac, bidClie.fchNac) &&
                Objects.equals(rfc, bidClie.rfc) &&
                Objects.equals(lugNac, bidClie.lugNac) &&
                Objects.equals(paisNac, bidClie.paisNac) &&
                Objects.equals(naci, bidClie.naci) &&
                Objects.equals(usrOpeCrea, bidClie.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidClie.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, nomClie, apePate, apeMate, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, 
        		geneClie, fchNac, rfc, lugNac, paisNac, naci, usrOpeCrea, usrOpeModi,idclierel);
    }
}
