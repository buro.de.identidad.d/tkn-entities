package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieTasaPK implements Serializable {
    private long idClie;
    private long idTipoTasa;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_tipo_tasa")
    @Id
    public long getIdTipoTasa() {
        return idTipoTasa;
    }

    public void setIdTipoTasa(long idTipoTasa) {
        this.idTipoTasa = idTipoTasa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieTasaPK that = (BidClieTasaPK) o;
        return idClie == that.idClie &&
                idTipoTasa == that.idTipoTasa;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idTipoTasa);
    }
}
