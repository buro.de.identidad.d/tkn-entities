package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidSelfPK implements Serializable {
    private long idClie;
    private long idSelf;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_self")
    @Id
    public long getIdSelf() {
        return idSelf;
    }

    public void setIdSelf(long idSelf) {
        this.idSelf = idSelf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidSelfPK bidSelfPK = (BidSelfPK) o;
        return idClie == bidSelfPK.idClie &&
                idSelf == bidSelfPK.idSelf;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idSelf);
    }
}
