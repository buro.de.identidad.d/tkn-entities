package com.teknei.bid.persistence.entities;
import javax.persistence.*;
import org.hibernate.annotations.Immutable;
import java.sql.Timestamp; 

@Entity
@Immutable
@Table(name = "bid_tipo_cliente", schema = "bid", catalog = "bid") 
public class BidClienteTipo 
{
    private Long id_tipo_cliente;
    private String cod_tipo_cliente;
    private String desc_tipo_cliente;
    private String estatus;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi; 
    
    @Id
    @Column(name = "id_tipo_cliente")
	public long getId_tipo_cliente() {
		return id_tipo_cliente;
    }
    @Basic
    @Column(name = "cod_tipo_cliente")
	public String getCod_tipo_cliente() {
		return cod_tipo_cliente;
	}
	@Basic
    @Column(name = "desc_tipo_cliente")
	public String getDesc_tipo_cliente() {
		return desc_tipo_cliente;
	}
	@Basic
    @Column(name = "estatus")
	public String getEstatus() {
		return estatus;
	}
	@Basic
    @Column(name = "usrCrea")
	public String getUsrCrea() {
		return usrCrea;
	}
	@Basic
    @Column(name = "fchCrea")
	public Timestamp getFchCrea() {
		return fchCrea;
	}
	@Basic
    @Column(name = "usrModi")
	public String getUsrModi() {
		return usrModi;
	}
	@Basic
    @Column(name = "fchModi")
	public Timestamp getFchModi() {
		return fchModi;
	}

	public void setId_tipo_cliente(long id_tipo_cliente) {
		this.id_tipo_cliente = id_tipo_cliente;
	}

	public void setCod_tipo_cliente(String cod_tipo_cliente) {
		this.cod_tipo_cliente = cod_tipo_cliente;
	}

	public void setDesc_tipo_cliente(String desc_tipo_cliente) {
		this.desc_tipo_cliente = desc_tipo_cliente;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public void setUsrCrea(String usrCrea) {
		this.usrCrea = usrCrea;
	}

	public void setFchCrea(Timestamp fchCrea) {
		this.fchCrea = fchCrea;
	}

	public void setUsrModi(String usrModi) {
		this.usrModi = usrModi;
	}

	public void setFchModi(Timestamp fchModi) {
		this.fchModi = fchModi;
	}
    
}

