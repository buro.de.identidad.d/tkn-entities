package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidBitacPK implements Serializable {
    private long idEmpr;
    private long idUsua;
    private long idDisp;
    private long idBita;

    @Column(name = "id_empr")
    @Id
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Column(name = "id_usua")
    @Id
    public long getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(long idUsua) {
        this.idUsua = idUsua;
    }

    @Column(name = "id_disp")
    @Id
    public long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(long idDisp) {
        this.idDisp = idDisp;
    }

    @Column(name = "id_bita")
    @Id
    public long getIdBita() {
        return idBita;
    }

    public void setIdBita(long idBita) {
        this.idBita = idBita;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidBitacPK that = (BidBitacPK) o;
        return idEmpr == that.idEmpr &&
                idUsua == that.idUsua &&
                idDisp == that.idDisp &&
                idBita == that.idBita;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, idUsua, idDisp, idBita);
    }
}
