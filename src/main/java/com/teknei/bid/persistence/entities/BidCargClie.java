package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_carg_clie", schema = "bid", catalog = "bid")
public class BidCargClie {
    private long idCargClie;
    private String nomArch;
    private String cadeClie;
    private Timestamp fchCarg;
    private long idStatus;
    private String detErr;
    private int idTipo;
    private int idEsta;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private Long idTipoCarg;

    @Id
    @Column(name = "id_carg_clie")
    public long getIdCargClie() {
        return idCargClie;
    }

    public void setIdCargClie(long idCargClie) {
        this.idCargClie = idCargClie;
    }

    @Basic
    @Column(name = "nom_arch")
    public String getNomArch() {
        return nomArch;
    }

    public void setNomArch(String nomArch) {
        this.nomArch = nomArch;
    }

    @Basic
    @Column(name = "cade_clie")
    public String getCadeClie() {
        return cadeClie;
    }

    public void setCadeClie(String cadeClie) {
        this.cadeClie = cadeClie;
    }

    @Basic
    @Column(name = "fch_carg")
    public Timestamp getFchCarg() {
        return fchCarg;
    }

    public void setFchCarg(Timestamp fchCarg) {
        this.fchCarg = fchCarg;
    }

    @Basic
    @Column(name = "id_status")
    public long getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(long idStatus) {
        this.idStatus = idStatus;
    }

    @Basic
    @Column(name = "det_err")
    public String getDetErr() {
        return detErr;
    }

    public void setDetErr(String detErr) {
        this.detErr = detErr;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "id_tipo_carg")
    public Long getIdTipoCarg() {
        return idTipoCarg;
    }

    public void setIdTipoCarg(Long idTipoCarg) {
        this.idTipoCarg = idTipoCarg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidCargClie that = (BidCargClie) o;
        return idCargClie == that.idCargClie &&
                idStatus == that.idStatus &&
                idTipo == that.idTipo &&
                idEsta == that.idEsta &&
                Objects.equals(nomArch, that.nomArch) &&
                Objects.equals(cadeClie, that.cadeClie) &&
                Objects.equals(fchCarg, that.fchCarg) &&
                Objects.equals(detErr, that.detErr) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(idTipoCarg, that.idTipoCarg);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCargClie, nomArch, cadeClie, fchCarg, idStatus, detErr, idTipo, idEsta, usrCrea, fchCrea, usrModi, fchModi, idTipoCarg);
    }
}
