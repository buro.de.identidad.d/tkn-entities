package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_pasa", schema = "bid", catalog = "bid")
@IdClass(BidCliePasaPK.class)
public class BidCliePasa {
    private long idClie;
    private long idPasa;
    private String numPasa;
    private String tipo;
    private String paisExpe;
    private String apel;
    private String nomb;
    private String naci;
    private Timestamp fchNac;
    private String sexo;
    private String lugNac;
    private Timestamp fchExpe;
    private Timestamp fchCadu;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_pasa")
    public long getIdPasa() {
        return idPasa;
    }

    public void setIdPasa(long idPasa) {
        this.idPasa = idPasa;
    }

    @Basic
    @Column(name = "num_pasa")
    public String getNumPasa() {
        return numPasa;
    }

    public void setNumPasa(String numPasa) {
        this.numPasa = numPasa;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "pais_expe")
    public String getPaisExpe() {
        return paisExpe;
    }

    public void setPaisExpe(String paisExpe) {
        this.paisExpe = paisExpe;
    }

    @Basic
    @Column(name = "apel")
    public String getApel() {
        return apel;
    }

    public void setApel(String apel) {
        this.apel = apel;
    }

    @Basic
    @Column(name = "nomb")
    public String getNomb() {
        return nomb;
    }

    public void setNomb(String nomb) {
        this.nomb = nomb;
    }

    @Basic
    @Column(name = "naci")
    public String getNaci() {
        return naci;
    }

    public void setNaci(String naci) {
        this.naci = naci;
    }

    @Basic
    @Column(name = "fch_nac")
    public Timestamp getFchNac() {
        return fchNac;
    }

    public void setFchNac(Timestamp fchNac) {
        this.fchNac = fchNac;
    }

    @Basic
    @Column(name = "sexo")
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "lug_nac")
    public String getLugNac() {
        return lugNac;
    }

    public void setLugNac(String lugNac) {
        this.lugNac = lugNac;
    }

    @Basic
    @Column(name = "fch_expe")
    public Timestamp getFchExpe() {
        return fchExpe;
    }

    public void setFchExpe(Timestamp fchExpe) {
        this.fchExpe = fchExpe;
    }

    @Basic
    @Column(name = "fch_cadu")
    public Timestamp getFchCadu() {
        return fchCadu;
    }

    public void setFchCadu(Timestamp fchCadu) {
        this.fchCadu = fchCadu;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidCliePasa that = (BidCliePasa) o;
        return idClie == that.idClie &&
                idPasa == that.idPasa &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(numPasa, that.numPasa) &&
                Objects.equals(tipo, that.tipo) &&
                Objects.equals(paisExpe, that.paisExpe) &&
                Objects.equals(apel, that.apel) &&
                Objects.equals(nomb, that.nomb) &&
                Objects.equals(naci, that.naci) &&
                Objects.equals(fchNac, that.fchNac) &&
                Objects.equals(sexo, that.sexo) &&
                Objects.equals(lugNac, that.lugNac) &&
                Objects.equals(fchExpe, that.fchExpe) &&
                Objects.equals(fchCadu, that.fchCadu) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idPasa, numPasa, tipo, paisExpe, apel, nomb, naci, fchNac, sexo, lugNac, fchExpe, fchCadu, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
