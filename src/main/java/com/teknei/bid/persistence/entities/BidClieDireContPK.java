package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieDireContPK implements Serializable {
    private long idClie;
    private long idDireCont;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_dire_cont")
    @Id
    public long getIdDireCont() {
        return idDireCont;
    }

    public void setIdDireCont(long idDireCont) {
        this.idDireCont = idDireCont;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieDireContPK that = (BidClieDireContPK) o;
        return idClie == that.idClie &&
                idDireCont == that.idDireCont;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idDireCont);
    }
}
