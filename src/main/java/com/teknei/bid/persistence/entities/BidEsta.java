package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_esta", schema = "bid", catalog = "bid")
@IdClass(BidEstaPK.class)
public class BidEsta {
    private String idPais;
    private String idEstd;
    private String nomEsta;

    @Id
    @Column(name = "id_pais")
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Id
    @Column(name = "id_estd")
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Basic
    @Column(name = "nom_esta")
    public String getNomEsta() {
        return nomEsta;
    }

    public void setNomEsta(String nomEsta) {
        this.nomEsta = nomEsta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEsta bidEsta = (BidEsta) o;
        return Objects.equals(idPais, bidEsta.idPais) &&
                Objects.equals(idEstd, bidEsta.idEstd) &&
                Objects.equals(nomEsta, bidEsta.nomEsta);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd, nomEsta);
    }
}
