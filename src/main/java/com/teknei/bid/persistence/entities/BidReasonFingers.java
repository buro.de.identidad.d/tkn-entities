package com.teknei.bid.persistence.entities;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity; 
import javax.persistence.Id; 
import javax.persistence.Table;
 

@Entity
@Table(name = "bid_reason_finger", schema = "bid", catalog = "bid")
public class BidReasonFingers {
    private long idreason;
    private String description;
    private String code;
    
    @Id  
    @Column(name = "idreason")
	public long getIdreason() {
		return idreason;
	}
	public void setIdreason(long idreason) {
		this.idreason = idreason;
	}
	@Basic
    @Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Basic
    @Column(name = "code") 
	public String getCode() {
		return code;
	}
	public void setCode(String code) 
	{
		this.code = code;
	}
}
