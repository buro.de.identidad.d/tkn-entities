package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidEstaPK implements Serializable {
    private String idPais;
    private String idEstd;

    @Column(name = "id_pais")
    @Id
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Column(name = "id_estd")
    @Id
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEstaPK bidEstaPK = (BidEstaPK) o;
        return Objects.equals(idPais, bidEstaPK.idPais) &&
                Objects.equals(idEstd, bidEstaPK.idEstd);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd);
    }
}
