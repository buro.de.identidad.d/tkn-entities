package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_ife_ine_bita", schema = "bid", catalog = "bid")
@IdClass(BidClieIfeIneBitaPK.class)
public class BidClieIfeIneBita {
    private long idClie;
    private long idIfe;
    private long idIfeIneBita;
    private String respUno;
    private String respDos;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_ife")
    public long getIdIfe() {
        return idIfe;
    }

    public void setIdIfe(long idIfe) {
        this.idIfe = idIfe;
    }

    @Id
    @SequenceGenerator(sequenceName = "bid.bid_clie_ife_ine_bita_id_ife_ine_bita_seq", name = "ine_bita_seq_name", allocationSize = 1)
    @GeneratedValue(generator = "ine_bita_seq_name", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_ife_ine_bita")
    public long getIdIfeIneBita() {
        return idIfeIneBita;
    }

    public void setIdIfeIneBita(long idIfeIneBita) {
        this.idIfeIneBita = idIfeIneBita;
    }

    @Basic
    @Column(name = "resp_uno")
    public String getRespUno() {
        return respUno;
    }

    public void setRespUno(String respUno) {
        this.respUno = respUno;
    }

    @Basic
    @Column(name = "resp_dos")
    public String getRespDos() {
        return respDos;
    }

    public void setRespDos(String respDos) {
        this.respDos = respDos;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieIfeIneBita that = (BidClieIfeIneBita) o;
        return idClie == that.idClie &&
                idIfe == that.idIfe &&
                idIfeIneBita == that.idIfeIneBita &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(respUno, that.respUno) &&
                Objects.equals(respDos, that.respDos) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idIfe, idIfeIneBita, respUno, respDos, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
