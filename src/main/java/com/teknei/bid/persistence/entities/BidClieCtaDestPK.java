package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieCtaDestPK implements Serializable {
    private long idClie;
    private String ctaClabDest;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "cta_clab_dest")
    @Id
    public String getCtaClabDest() {
        return ctaClabDest;
    }

    public void setCtaClabDest(String ctaClabDest) {
        this.ctaClabDest = ctaClabDest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCtaDestPK that = (BidClieCtaDestPK) o;
        return idClie == that.idClie &&
                Objects.equals(ctaClabDest, that.ctaClabDest);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, ctaClabDest);
    }
}
