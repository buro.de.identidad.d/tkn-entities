package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_disp", schema = "bid", catalog = "bid")
public class BidDisp {
    private long idDisp;
    private String descDisp;
    private String numSeri;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @SequenceGenerator(schema = "bid", name = "BID_DISP_SEQ", sequenceName = "bid.bid_disp_id_disp_seq", allocationSize = 1, catalog = "bid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_DISP_SEQ")
    @Column(name = "id_disp")
    public long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(long idDisp) {
        this.idDisp = idDisp;
    }

    @Basic
    @Column(name = "desc_disp")
    public String getDescDisp() {
        return descDisp;
    }

    public void setDescDisp(String descDisp) {
        this.descDisp = descDisp;
    }

    @Basic
    @Column(name = "num_seri")
    public String getNumSeri() {
        return numSeri;
    }

    public void setNumSeri(String numSeri) {
        this.numSeri = numSeri;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidDisp bidDisp = (BidDisp) o;
        return idDisp == bidDisp.idDisp &&
                idEsta == bidDisp.idEsta &&
                idTipo == bidDisp.idTipo &&
                Objects.equals(descDisp, bidDisp.descDisp) &&
                Objects.equals(numSeri, bidDisp.numSeri) &&
                Objects.equals(usrCrea, bidDisp.usrCrea) &&
                Objects.equals(fchCrea, bidDisp.fchCrea) &&
                Objects.equals(usrModi, bidDisp.usrModi) &&
                Objects.equals(fchModi, bidDisp.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDisp, descDisp, numSeri, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
