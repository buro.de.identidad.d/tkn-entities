package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieRfisPK implements Serializable {
    private long idClie;
    private String idPais;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_pais")
    @Id
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieRfisPK that = (BidClieRfisPK) o;
        return idClie == that.idClie &&
                Objects.equals(idPais, that.idPais);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idPais);
    }
}
