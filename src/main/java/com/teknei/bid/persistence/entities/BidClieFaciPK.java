package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieFaciPK implements Serializable {
    private long idClie;
    private long idFaci;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_faci")
    @Id
    public long getIdFaci() {
        return idFaci;
    }

    public void setIdFaci(long idFaci) {
        this.idFaci = idFaci;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieFaciPK that = (BidClieFaciPK) o;
        return idClie == that.idClie &&
                idFaci == that.idFaci;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idFaci);
    }
}
