package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidLocaPK implements Serializable {
    private String idPais;
    private String idEstd;
    private String idMuni;
    private String idLoca;

    @Column(name = "id_pais")
    @Id
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Column(name = "id_estd")
    @Id
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Column(name = "id_muni")
    @Id
    public String getIdMuni() {
        return idMuni;
    }

    public void setIdMuni(String idMuni) {
        this.idMuni = idMuni;
    }

    @Column(name = "id_loca")
    @Id
    public String getIdLoca() {
        return idLoca;
    }

    public void setIdLoca(String idLoca) {
        this.idLoca = idLoca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidLocaPK bidLocaPK = (BidLocaPK) o;
        return Objects.equals(idPais, bidLocaPK.idPais) &&
                Objects.equals(idEstd, bidLocaPK.idEstd) &&
                Objects.equals(idMuni, bidLocaPK.idMuni) &&
                Objects.equals(idLoca, bidLocaPK.idLoca);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd, idMuni, idLoca);
    }
}
