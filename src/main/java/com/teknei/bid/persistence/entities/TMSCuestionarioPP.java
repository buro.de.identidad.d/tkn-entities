package com.teknei.bid.persistence.entities;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bid_tms", schema = "bid", catalog = "bid")
public class TMSCuestionarioPP {
    private long idClie;
    private String data;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "data")
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TMSCuestionarioPP bidTel = (TMSCuestionarioPP) o;
        return idClie == bidTel.idClie &&
        		data == bidTel.data;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, data);
    }
}
