package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_tel", schema = "bid", catalog = "bid")
@IdClass(BidTelPK.class)
public class BidTel {
    private long idClie;
    private String tele;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "tele")
    public String getTele() {
        return tele;
    }

    public void setTele(String tele) {
        this.tele = tele;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidTel bidTel = (BidTel) o;
        return idClie == bidTel.idClie &&
                idEsta == bidTel.idEsta &&
                idTipo == bidTel.idTipo &&
                Objects.equals(tele, bidTel.tele) &&
                Objects.equals(usrCrea, bidTel.usrCrea) &&
                Objects.equals(fchCrea, bidTel.fchCrea) &&
                Objects.equals(usrModi, bidTel.usrModi) &&
                Objects.equals(fchModi, bidTel.fchModi) &&
                Objects.equals(usrOpeCrea, bidTel.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidTel.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, tele, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
