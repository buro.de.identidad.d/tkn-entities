package com.teknei.bid.persistence.entities;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "bid_clie_tel", schema = "bid", catalog = "bid")
public class BidClieTel {
	private Long idClie;
    private String tele;
	

    @Basic
    @Id
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "tele")
    public String getTel() {
        return tele;
    }

    public void setTel(String tele) {
        this.tele = tele;
    }
    
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieTel that = (BidClieTel) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(tele, that.tele);
}
    @Override
    public int hashCode() {

        return Objects.hash(idClie, tele);
        		}
}
