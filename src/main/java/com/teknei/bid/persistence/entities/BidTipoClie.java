package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_tipo_clie", schema = "bid", catalog = "bid")
public class BidTipoClie {
    private long idTipoClie;
    private String codTipoClie;
    private String descTipoClie;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_tipo_clie")
    public long getIdTipoClie() {
        return idTipoClie;
    }

    public void setIdTipoClie(long idTipoClie) {
        this.idTipoClie = idTipoClie;
    }

    @Basic
    @Column(name = "cod_tipo_clie")
    public String getCodTipoClie() {
        return codTipoClie;
    }

    public void setCodTipoClie(String codTipoClie) {
        this.codTipoClie = codTipoClie;
    }

    @Basic
    @Column(name = "desc_tipo_clie")
    public String getDescTipoClie() {
        return descTipoClie;
    }

    public void setDescTipoClie(String descTipoClie) {
        this.descTipoClie = descTipoClie;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidTipoClie that = (BidTipoClie) o;
        return idTipoClie == that.idTipoClie &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(codTipoClie, that.codTipoClie) &&
                Objects.equals(descTipoClie, that.descTipoClie) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTipoClie, codTipoClie, descTipoClie, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
