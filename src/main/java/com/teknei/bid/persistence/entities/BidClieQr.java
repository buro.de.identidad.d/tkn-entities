package com.teknei.bid.persistence.entities;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "bid_clie_qr", schema = "bid", catalog = "bid")
public class BidClieQr 
{
	 private Long idClieQr;
	    private Long idClie;
	    private String qr;
	    private Integer idEstaQr;
	    private Timestamp fchVenc;
	    private Integer idEsta;
	    private Integer idTipo;
	    private String usrCrea;
	    private Timestamp fchCrea;
	    private String usrModi;
	    private Timestamp fchModi;
	    private String usrOpeModi;
	    private String usrOpeCrea;

	    public BidClieQr()
	    {
	    }
	    @Basic
	    @Id
	    @Column(name = "id_clie_qr")
	    public Long getIdClieQr()
	    {
	        return idClieQr;
	    }

	    public void setIdClieQr(Long idClieQr)
	    {
	        this.idClieQr = idClieQr;
	    }
	    @Basic 
	    @Column(name = "id_clie")
	    public Long getIdClie()
	    {
	        return idClie;
	    }

	    public void setIdClie(Long idClie)
	    {
	        this.idClie = idClie;
	    }
	    @Basic 
	    @Column(name = "qr")
	    public String getQr()
	    {
	        return qr;
	    }

	    public void setQr(String qr)
	    {
	        this.qr = qr;
	    }
	    @Basic 
	    @Column(name = "id_esta_qr")
	    public Integer getIdEstaQr()
	    {
	        return idEstaQr;
	    }

	    public void setIdEstaQr(Integer idEstaQr)
	    {
	        this.idEstaQr = idEstaQr;
	    }
	    @Basic 
	    @Column(name = "fch_venc")
	    public Timestamp getFchVenc()
	    {
	        return fchVenc;
	    }

	    public void setFchVenc(Timestamp fchVenc)
	    {
	        this.fchVenc = fchVenc;
	    }
	    @Basic 
	    @Column(name = "id_esta")
	    public Integer getIdEsta()
	    {
	        return idEsta;
	    }

	    public void setIdEsta(Integer idEsta)
	    {
	        this.idEsta = idEsta;
	    }
	    @Basic 
	    @Column(name = "id_tipo")	
	    public Integer getIdTipo()
	    {
	        return idTipo;
	    }

	    public void setIdTipo(Integer idTipo)
	    {
	        this.idTipo = idTipo;
	    }
	    @Basic 
	    @Column(name = "usr_crea")
	    public String getUsrCrea()
	    {
	        return usrCrea;
	    }

	    public void setUsrCrea(String usrCrea)
	    {
	        this.usrCrea = usrCrea;
	    }
	    @Basic 
	    @Column(name = "fch_crea")
	    public Timestamp getFchCrea()
	    {
	        return fchCrea;
	    }

	    public void setFchCrea(Timestamp fchCrea)
	    {
	        this.fchCrea = fchCrea;
	    }
	    @Basic 
	    @Column(name = "usr_modi")
	    public String getUsrModi()
	    {
	        return usrModi;
	    }

	    public void setUsrModi(String usrModi)
	    {
	        this.usrModi = usrModi;
	    }
	    @Basic 
	    @Column(name = "fch_modi")
	    public Timestamp getFchModi()
	    {
	        return fchModi;
	    }

	    public void setFchModi(Timestamp fchModi)
	    {
	        this.fchModi = fchModi;
	    }
	    @Basic 
	    @Column(name = "usr_ope_modi")
	    public String getUsrOpeModi()
	    {
	        return usrOpeModi;
	    }

	    public void setUsrOpeModi(String usrOpeModi)
	    {
	        this.usrOpeModi = usrOpeModi;
	    }
	    @Basic 
	    @Column(name = "usr_ope_crea")
	    public String getUsrOpeCrea()
	    {
	        return usrOpeCrea;
	    }

	    public void setUsrOpeCrea(String usrOpeCrea)
	    {
	        this.usrOpeCrea = usrOpeCrea;
	    }

	    public boolean equals(Object o)
	    {
	        if(this == o)
	        {
	            return true;
	        }
	        if(o == null || getClass() != o.getClass())
	        {
	            return false;
	        } else
	        {
	            BidClieQr bidClieQr = (BidClieQr)o;
	            return  Objects.equals(idClieQr, bidClieQr.idClieQr) && 
	            		Objects.equals(idClie, bidClieQr.idClie) && 
	            		Objects.equals(qr, bidClieQr.qr) && 
	            		Objects.equals(idEstaQr, bidClieQr.idEstaQr) &&
	            		Objects.equals(fchVenc, bidClieQr.fchVenc) && 
	            		Objects.equals(idEsta, bidClieQr.idEsta) && 
	            		Objects.equals(idTipo, bidClieQr.idTipo) && 
	            		Objects.equals(usrCrea, bidClieQr.usrCrea) && 
	            		Objects.equals(fchCrea, bidClieQr.fchCrea) && 
	            		Objects.equals(usrModi, bidClieQr.usrModi) && 
	            		Objects.equals(fchModi, bidClieQr.fchModi) && 
	            		Objects.equals(usrOpeModi, bidClieQr.usrOpeModi) && 
	            		Objects.equals(usrOpeCrea, bidClieQr.usrOpeCrea);
	        }
	    }

	    public int hashCode()
	    {
	        return Objects.hash(new Object[] {
	            idClieQr, idClie, qr, idEstaQr, fchVenc, idEsta, idTipo, usrCrea, fchCrea, usrModi, 
	            fchModi, usrOpeModi, usrOpeCrea
	        });
	    }
}
