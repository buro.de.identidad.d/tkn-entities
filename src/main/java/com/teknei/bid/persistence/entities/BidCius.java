package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_cius", schema = "bid", catalog = "bid")
public class BidCius {
    private long idCius;
    private String nomCius;

    @Id
    @Column(name = "id_cius")
    public long getIdCius() {
        return idCius;
    }

    public void setIdCius(long idCius) {
        this.idCius = idCius;
    }

    @Basic
    @Column(name = "nom_cius")
    public String getNomCius() {
        return nomCius;
    }

    public void setNomCius(String nomCius) {
        this.nomCius = nomCius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidCius bidCius = (BidCius) o;
        return idCius == bidCius.idCius &&
                Objects.equals(nomCius, bidCius.nomCius);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCius, nomCius);
    }
}
