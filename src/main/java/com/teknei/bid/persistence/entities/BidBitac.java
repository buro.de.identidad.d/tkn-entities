package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_bitac", schema = "bid", catalog = "bid")
@IdClass(BidBitacPK.class)
public class BidBitac {
    private long idEmpr;
    private long idUsua;
    private long idDisp;
    private long idBita;
    private Boolean intFall;
    private String usrCrea;
    private Timestamp fchCrea;

    @Id
    @Column(name = "id_empr")
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Id
    @Column(name = "id_usua")
    public long getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(long idUsua) {
        this.idUsua = idUsua;
    }

    @Id
    @Column(name = "id_disp")
    public long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(long idDisp) {
        this.idDisp = idDisp;
    }

    @Id
    @Column(name = "id_bita")
    public long getIdBita() {
        return idBita;
    }

    public void setIdBita(long idBita) {
        this.idBita = idBita;
    }

    @Basic
    @Column(name = "int_fall")
    public Boolean getIntFall() {
        return intFall;
    }

    public void setIntFall(Boolean intFall) {
        this.intFall = intFall;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidBitac bidBitac = (BidBitac) o;
        return idEmpr == bidBitac.idEmpr &&
                idUsua == bidBitac.idUsua &&
                idDisp == bidBitac.idDisp &&
                idBita == bidBitac.idBita &&
                Objects.equals(intFall, bidBitac.intFall) &&
                Objects.equals(usrCrea, bidBitac.usrCrea) &&
                Objects.equals(fchCrea, bidBitac.fchCrea);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, idUsua, idDisp, idBita, intFall, usrCrea, fchCrea);
    }
}
