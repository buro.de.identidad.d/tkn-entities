package com.teknei.bid.persistence.entities;

import java.sql.Timestamp;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="bid_clie_curp", schema="bid", catalog="bid")
@IdClass(BidClieCurpPK.class)
public class BidClieCurp
{
  private long idClie;
  private String curp;
  private int idTipo;
  private int idEsta;
  private String usrCrea;
  private Timestamp fchCrea;
  private String usrModi;
  private Timestamp fchModi;
  private String usrOpeCrea;
  private String usrOpeModi;
  private long rel;
  
  @Id
  @Column(name="id_clie")
  public long getIdClie()
  {
    return this.idClie;
  }
  
  public void setIdClie(long idClie)
  {
    this.idClie = idClie;
  }
  
  @Id
  @Column(name="curp")
  public String getCurp()
  {
    return this.curp;
  }
  
  public void setCurp(String curp)
  {
    this.curp = curp;
  }
  
  @Basic
  @Column(name="id_tipo")
  public int getIdTipo()
  {
    return this.idTipo;
  }
  
  public void setIdTipo(int idTipo)
  {
    this.idTipo = idTipo;
  }
  
  @Basic
  @Column(name="id_esta")
  public int getIdEsta()
  {
    return this.idEsta;
  }
  
  public void setIdEsta(int idEsta)
  {
    this.idEsta = idEsta;
  }
  
  @Basic
  @Column(name="usr_crea")
  public String getUsrCrea()
  {
    return this.usrCrea;
  }
  
  public void setUsrCrea(String usrCrea)
  {
    this.usrCrea = usrCrea;
  }
  
  @Basic
  @Column(name="fch_crea")
  public Timestamp getFchCrea()
  {
    return this.fchCrea;
  }
  
  public void setFchCrea(Timestamp fchCrea)
  {
    this.fchCrea = fchCrea;
  }
  
  @Basic
  @Column(name="usr_modi")
  public String getUsrModi()
  {
    return this.usrModi;
  }
  
  public void setUsrModi(String usrModi)
  {
    this.usrModi = usrModi;
  }
  
  @Basic
  @Column(name="fch_modi")
  public Timestamp getFchModi()
  {
    return this.fchModi;
  }
  
  public void setFchModi(Timestamp fchModi)
  {
    this.fchModi = fchModi;
  }
  
  @Basic
  @Column(name="usr_ope_crea")
  public String getUsrOpeCrea()
  {
    return this.usrOpeCrea;
  }
  
  public void setUsrOpeCrea(String usrOpeCrea)
  {
    this.usrOpeCrea = usrOpeCrea;
  }
  
  @Basic
  @Column(name="usr_ope_modi")
  public String getUsrOpeModi()
  {
    return this.usrOpeModi;
  }
  
  public void setUsrOpeModi(String usrOpeModi)
  {
    this.usrOpeModi = usrOpeModi;
  }
  
  @Basic
  @Column(name="rel")
  public long getRel()
  {
    return this.rel;
  }
  
  public void setRel(long rel)
  {
    this.rel = rel;
  }
  
  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }
    BidClieCurp that = (BidClieCurp)o;
    if ((this.idClie == that.idClie) && (this.idTipo == that.idTipo) && (this.idEsta == that.idEsta) && (this.rel == that.rel)) {}
    return 
    
      (Objects.equals(this.curp, that.curp)) && 
      (Objects.equals(this.usrCrea, that.usrCrea)) && 
      (Objects.equals(this.fchCrea, that.fchCrea)) && 
      (Objects.equals(this.usrModi, that.usrModi)) && 
      (Objects.equals(this.fchModi, that.fchModi)) && 
      (Objects.equals(this.usrOpeCrea, that.usrOpeCrea)) && 
      (Objects.equals(this.usrOpeModi, that.usrOpeModi));
  }
  
  public int hashCode()
  {
    return Objects.hash(new Object[] { Long.valueOf(this.idClie), this.curp, Integer.valueOf(this.idTipo), Integer.valueOf(this.idEsta), this.usrCrea, this.fchCrea, this.usrModi, this.fchModi, this.usrOpeCrea, this.usrOpeModi, Long.valueOf(this.rel) });
  }
}
